import argparse
from scipy.io import loadmat
import matplotlib.pyplot as plt
import numpy as np
import os


def get_distances(valfile):
    f = loadmat(valfile)
    
    preds = f['preds']
    all_dist = []
    avg_dist = []
    for pred in preds:
        dist = []
        for joint in pred:
            dist.append(np.linalg.norm((joint[0] - joint[2])**2 + (joint[1] - joint[3])**2))
        
        all_dist.append(dist)
        
    return all_dist


def main_plot(vallst):
    for valfile in vallst:
        valname = os.path.basename(os.path.dirname(valfile))
        distances = get_distances(valfile)
        
        
        for ind_dist in distances:
            normal_dist = 0
            print(ind_dist)
            if ind_dist[12] != 0 and ind_dist[11] != 0:
                normal_dist = abs(ind_dist[12] - ind_dist[11])
            elif ind_dist[13] != 0 and ind_dist[14] != 0:
                normal_dist = abs(ind_dist[13] - ind_dist[14])
            elif ind_dist[2] != 0 and ind_dist[1] != 0:
                normal_dist = abs(ind_dist[2] - ind_dist[1])
                normal_dist /= 1.5
            elif ind_dist[3] != 0 and ind_dist[4] != 0:
                normal_dist = abs(ind_dist[3] - ind_dist[4])
                normal_dist /= 1.5
            else:
                normal_dist = 75
                
            for i in ind_dist:
                i /= normal_dist
    
            
    #remove occluded joints
    _dist = []
    for ind_dist in distances:
        for i in ind_dist:
            if i != 0:
                _dist.append(i)

    avg_distance = sum(_dist) / len(_dist)
    
    binary_classification = []
    for dist in distances:
        for ind_dist in dist:
            if ind_dist > avg_distance:
                binary_classification.append(0)
            else:
                binary_classification.append(1)

    good_scores = sum(binary_classification)
    print('% scores within 1ft')
    total_percents = good_scores / len(binary_classification)
    print(total_percents)
    
    avg_distance /= 2
    
    binary_classification = []
    for dist in distances:
        for ind_dist in dist:
            if ind_dist > avg_distance:
                binary_classification.append(0)
            else:
                binary_classification.append(1)

    good_scores = sum(binary_classification)
    print('% scores within 1/2ft')
    total_half_percents = good_scores / len(binary_classification)
    print(total_half_percents)
    distances = np.array(distances)
    avg_distances_by_joint = []
    
    joint_percents = [total_percents]
    joint_half_percents = [total_half_percents]
    
    for joint in range(distances.shape[1]):
        avg_distance_by_joint = sum(distances[:, joint]) / len(distances[:, joint])
        binary_classification = []
        for i in distances[:, joint]:
            if i > avg_distance_by_joint:
                binary_classification.append(0)
            else:
                binary_classification.append(1)

        good_scores = sum(binary_classification)
        print(f'joint #{joint}')
        print('% scores within 1ft')
        print(good_scores / len(binary_classification))
        joint_percents.append(good_scores / len(binary_classification))
        
        avg_distance_by_joint /= 2
        binary_classification = []
        for i in distances[:, joint]:
            if i > avg_distance_by_joint:
                binary_classification.append(0)
            else:
                binary_classification.append(1)

        good_scores = sum(binary_classification)
        print('% scores within 1/2ft')
        print(good_scores / len(binary_classification))
        joint_half_percents.append(good_scores / len(binary_classification))
        
    col_headers = ('Joint', '% in 1ft', '% in 1/2ft')
    row_headers = ('Total', 'R ankle', 'R knee', 'R hip', 'L hip', 'L knee',
                   'L ankle', 'pelvis', 'Thorax', 'U neck', 'Head', 'R wrist',
                   'R elbow', 'R shoulder', 'L shoulder', 'L elbow', 'L wrist')
    
    cell_text = []
    for n in range(len(row_headers)):
        cell_text.append([f'%2.2f' % (joint_percents[n] * 100), f'%2.2f' % (joint_half_percents[n] * 100)])
    
    print(cell_text)
    plt.table(cellText=cell_text,
              rowLabels=row_headers,
              colLabels=col_headers)
    
    plt.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--val_file", '--list', action='append', help='val file with validation score')

    args = parser.parse_args()
    main_plot(args.val_file)
