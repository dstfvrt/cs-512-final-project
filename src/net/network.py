import sys

sys.path.insert(0, "../data_gen/")
sys.path.insert(0, "../eval/")

from model import create_hourglass
from mpii_datagen import MPIIDataGen


class HourglassNetwork(object):

    def __init__(self, num_classes, num_stacks, num_channels, inres, outres, activation, kernel_size, stride):
        self.num_classes = num_classes
        self.num_stacks = num_stacks
        self.num_channels = num_channels
        self.inres = inres
        self.outres = outres
        self.activation = activation
        self.kernel_size = kernel_size
        self.stride = stride

    def build_model(self, mobile=False, show=False):
        self.model = create_hourglass(self.num_classes, self.num_stacks,
                                      self.num_channels, self.inres, self.outres,
                                      self.activation, self.kernel_size, self.stride)

        if show:
            self.model.summary()

    def train(self, batch_size, model_path, epochs):
        #TODO: read in john's images
        train_dataset = MPIIDataGen("data/mpii/mpii_annotations.json", "data/mpii/images",
                                    inres=self.inres, outres=self.outres, is_train=True)
        train_gen = train_dataset.generator(batch_size, self.num_stacks, sigma=1, is_shuffle=True,
                                            rot_flag=True, scale_flag=True, flip_flag=True)

        hist = self.model.fit(train_gen, steps_per_epoch=train_dataset.get_dataset_size() // batch_size,
                                 epochs=epochs)
        self.model.save(model_path)
        
        return hist