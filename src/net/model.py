from keras.models import *
from keras.layers import *
from keras.optimizers import Adam, RMSprop
from keras.losses import mean_squared_error
from keras.metrics import AUC
import keras.backend as K

def create_hourglass(num_classes, num_stacks, num_channels, inres, outres, a_func, ks, stride):

    input = Input(shape=(inres[0], inres[1], 3))
    
    # front module, input to 1/4 resolution
    # 1 kernel_size conv + maxpooling
    # 3 residuals
    
    _x = Conv2D(64, kernel_size=(7, 7), strides=stride, padding='same', activation=a_func, name='front_conv_1x1_x1')(
        input)
    _x = BatchNormalization()(_x)

    _x = bottleneck(_x, num_channels // 2, a_func, ks, 'front_residual_x1')
    _x = MaxPool2D(pool_size=(2, 2), strides=(2, 2))(_x)

    _x = bottleneck(_x, num_channels // 2, a_func, ks, 'front_residual_x2')
    _x = bottleneck(_x, num_channels, a_func, ks, 'front_residual_x3')

    head_next_stage = _x
    
    outputs = []
    for i in range(num_stacks):
        head_next_stage, head_to_loss = hourglass(head_next_stage, num_classes, num_channels, a_func, ks, stride, i)
        outputs.append(head_to_loss)

    model = Model(inputs=input, outputs=outputs)
    rms = RMSprop(lr=0.0004)
    model.compile(optimizer=rms, loss=mean_squared_error, metrics=["accuracy", AUC()])

    return model

def hourglass(start, num_classes, num_channels, a_func, ks, stride, hgid):
    # create encoder blocks for hourglass module
    # ef1, ef2, ef4 , ef8 : 1, 1/2, 1/4 1/8 resolution

    hgname = 'hg' + str(hgid)

    ef1 = bottleneck(start, num_channels, a_func, ks, hgname + '_e1')
    _x = MaxPool2D(pool_size=(2, 2), strides=(2, 2))(ef1)

    ef2 = bottleneck(_x, num_channels, a_func, ks, hgname + '_e2')
    _x = MaxPool2D(pool_size=(2, 2), strides=(2, 2))(ef2)

    ef4 = bottleneck(_x, num_channels, a_func, ks, hgname + '_e4')
    _x = MaxPool2D(pool_size=(2, 2), strides=(2, 2))(ef4)

    ef8 = bottleneck(_x, num_channels, a_func, ks, hgname + '_e8')

    ef8_connect = bottleneck(ef8, num_channels, a_func, ks, hgname + "_ef8")

    #decoder blocks

    _x = bottleneck(ef8, num_channels, a_func, ks, str(hgid) + "_ef8_x1")
    _x = bottleneck(_x, num_channels, a_func, ks, str(hgid) + "_ef8_x2")
    _x = bottleneck(_x, num_channels, a_func, ks, str(hgid) + "_ef8_x3")

    df8 = Add()([_x, ef8_connect])

    df4 = decoder(ef4, df8, hgname + '_df4', a_func, ks, num_channels)

    df2 = decoder(ef2, df4, hgname + '_df2', a_func, ks, num_channels)

    df1 = decoder(ef1, df2, hgname + '_df1', a_func, ks, num_channels)

    #hg heads
    #head_1. to next stage
    #head_2. to intermediate features
    head_1 = Conv2D(num_channels, kernel_size=(1, 1), activation=a_func, padding='same', name=str(hgid) + '_conv_1x1_x1')(df1)
    head_1 = BatchNormalization()(head_1)

    # for head as intermediate supervision, use 'linear' as activation.
    head_2 = Conv2D(num_classes, kernel_size=(1, 1), activation='linear', padding='same',
                        name=str(hgid) + '_conv_1x1_parts')(head_1)

    # use linear activation
    head_1 = Conv2D(num_channels, kernel_size=(1, 1), activation='linear', padding='same',
                  name=str(hgid) + '_conv_1x1_x2')(head_1)
    head_m = Conv2D(num_channels, kernel_size=(1, 1), activation='linear', padding='same',
                    name=str(hgid) + '_conv_1x1_x3')(head_2)
    head_1 = Add()([head_1, head_m, start])
    return head_1, head_2

def bottleneck(start, num_out_channels, a_func, ks, block_name):
    # skip layer
    if K.int_shape(start)[-1] == num_out_channels:
        _skip = start
    else:
        _skip = Conv2D(num_out_channels, kernel_size=(1, 1), activation='relu', padding='same',
                       name=block_name + 'skip')(start)

    # residual: 3 conv blocks,  [num_out_channels/2  -> num_out_channels/2 -> num_out_channels]
    _x = Conv2D(num_out_channels / 2, kernel_size=(1, 1), activation=a_func, padding='same',
                name=block_name + '_conv_1x1_x1')(start)
    _x = BatchNormalization()(_x)
    _x = Conv2D(num_out_channels / 2, kernel_size=ks, activation=a_func, padding='same',
                name=block_name + '_conv_x_x2')(_x)
    _x = BatchNormalization()(_x)
    _x = Conv2D(num_out_channels, kernel_size=(1, 1), activation=a_func, padding='same',
                name=block_name + '_conv_1x1_x3')(_x)
    _x = BatchNormalization()(_x)
    _x = Add(name=block_name + '_residual')([_skip, _x])

    return _x

def decoder(left, right, name, a_func, ks, num_channels):
    '''
    :param left: connect left feature to right feature
    :param name: layer name
    :return:
    '''
    # left -> 1 bottlenect
    # right -> upsampling
    # Add   -> left + right

    _xleft = bottleneck(left, num_channels, a_func, ks, name + '_connect')
    _xright = UpSampling2D()(right)
    add = Add()([_xleft, _xright])
    out = bottleneck(add, num_channels, a_func, ks, name + '_connect2')
    
    return out
