import sys

sys.path.insert(0, "src/data_gen/")
sys.path.insert(0, "src/net/")

import argparse
import tensorflow as tf
from network import HourglassNetwork

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--batch_size", default=8, type=int, help='batch size for training')
    parser.add_argument("--model_path", help='path to store trained model')
    parser.add_argument("--num_stack", default=2, type=int, help='num of stacks')
    parser.add_argument("--epochs", default=20, type=int, help="number of traning epochs")
    parser.add_argument("--activation", default='relu', type=str, help="activation used for deep layers")
    parser.add_argument("--kernel_size", default=(3, 3), type=tuple, help="size of convolution kernel")
    parser.add_argument("--stride", default=(2, 2), type=tuple, help="stride used by convolution kernels")

    args = parser.parse_args()

    #allocate memory as needed
    physical_devices = tf.config.list_physical_devices('GPU')
    try:
        tf.config.experimental.set_memory_growth(physical_devices[0], True)
    except:
      # Invalid device or cannot modify virtual devices once initialized.
      pass
  
    #avoids image classification bug in tensorflow:
    #https://github.com/tensorflow/tensorflow/issues/38503
    tf.compat.v1.disable_eager_execution()

    xnet = HourglassNetwork(num_classes=16, num_stacks=args.num_stack, num_channels=256, inres=(256, 256),
                            outres=(64, 64), activation= args.activation, kernel_size=args.kernel_size, stride=args.stride)

    xnet.build_model(show=True)
    hist = xnet.train(epochs=args.epochs, model_path=args.model_path, batch_size=args.batch_size)
    history_dict = hist.history
    acc = history_dict['accuracy']
    auc = history_dict['auc']
    loss = history_dict['loss']
    epochs = range(1, args.epochs)
    #print results
    import matplotlib.pyplot as plt
    #Plot loss
    plt.plot(epochs, loss, 'bo', label='Training loss')
    plt.title('Training loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.show()
    #Plot auc
    plt.clf()
    plt.plot(epochs, acc, 'bo', label='Training acc')
    plt.title('Training AUC')
    plt.xlabel('Epochs')
    plt.ylabel('AUC')
    plt.legend()
    plt.show()