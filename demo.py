import sys

sys.path.insert(0, "src/data_gen/")
sys.path.insert(0, "src/net/")
sys.path.insert(0, "src/eval/")

import os
import numpy as np
import argparse
from heatmap_process import post_process_heatmap
from mpii_datagen import MPIIDataGen
from data_process import normalize
import keras
import cv2 as cv

global xnet

def inference_rgb(rgbdata, orgshape, mean=None):

    scale = (orgshape[0] * 1.0 / 256, orgshape[1] * 1.0 / 256)
    imgdata = cv.resize(rgbdata, (256, 256))

    if mean is None:
        mean = np.array([0.4404, 0.4440, 0.4327], dtype=np.float)

    imgdata = normalize(imgdata, mean)

    input = imgdata[np.newaxis, :, :, :]
    
    global xnet
    out = xnet.predict(input)
    return out[-1], scale

def inference_file(imgfile, mean=None):
    imgdata = cv.imread(imgfile)
    ret = inference_rgb(imgdata, imgdata.shape, mean)
    return ret

def render_joints(cvmat, joints, conf_th=0.2):
    for _joint in joints:
        _x, _y, _conf = _joint
        if _conf > conf_th:
            cv.circle(cvmat, center=(int(_x), int(_y)), color=(255, 0, 0), radius=7, thickness=2)

    return cvmat


def main_inference(modelfile, imgfile, confth):
    global xnet
    xnet = keras.models.load_model(modelfile)

    out, scale = inference_file(imgfile)

    kps = post_process_heatmap(out[0, :, :, :])

    ignore_kps = ['plevis', 'thorax', 'head_top']
    kp_keys = MPIIDataGen.get_kp_keys()
    mkps = list()
    for i, _kp in enumerate(kps):
        if kp_keys[i] in ignore_kps:
            _conf = 0.0
        else:
            _conf = _kp[2]
        mkps.append((_kp[0] * scale[1] * 4, _kp[1] * scale[0] * 4, _conf))

    cvmat = render_joints(cv.imread(imgfile), mkps, confth)

    cv.imshow('Results', cvmat)
    cv.waitKey()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--gpuID", default=0, type=int, help='gpu id')
    parser.add_argument("--model_path", help='path to store trained model')
    parser.add_argument("--num_stack", type=int, help='num of stack')
    parser.add_argument("--input_image", help='input image file')
    parser.add_argument("--conf_threshold", type=float, default=0.2, help='confidence threshold')

    args = parser.parse_args()

    main_inference(modelfile=args.model_path, imgfile=args.input_image, confth=args.conf_threshold)
