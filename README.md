# Stacked Hourglass networks in Keras

  A Keras implementation of:
  https://arxiv.org/pdf/1603.06937.pdf

## Project Structure
- `data` : data folder, mpii, demo
- `models` : trained models
- `src` : src code
`src/data_gen` : data generator, augmentation and processing code, from [Stacked_Hourglass_Network_Keras](https://github.com/yuanyuanli85/Stacked_Hourglass_Network_Keras)
`src/eval` : evaluation code, generates ground truth heatmaps, from  [Stacked_Hourglass_Network_Keras](https://github.com/yuanyuanli85/Stacked_Hourglass_Network_Keras)
`src/net` : model and network definitions
`src/tools`: tool to draw accuracy curve
`./demo.py` : demos a trained network.
`./eval.py` : performs validation on a trained network.
`./train.py` : trains a network with the given arguments (epoch, batch size, etc).

## Demo
  ```
  python .\demo.py --model_path .\models\i2\ --input_image .\data\demo\demo_image.jpg --conf_threshold 0.1
  ```

## Training
  ```
  python train.py --batch_size 24, --model_path models/i3 --num_stack 1 --epochs 5
  ```

## Eval
 ```
 python .\eval.py --model_path ../../models/i2 --num_stack 2
 ```

## get MPII data
- Download MPII Dataset and put its images under `data/mpii/images`
- The json `mpii_annotations.json` contains all of images' annotations including train and validation.