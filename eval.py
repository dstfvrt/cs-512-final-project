import sys

sys.path.insert(0, "src/data_gen/")
sys.path.insert(0, "src/net/")
sys.path.insert(0, "src/eval/")

import numpy as np
from scipy.io import savemat
import keras
from mpii_datagen import MPIIDataGen
from eval_heatmap import get_predicted_kp_from_htmap
import argparse


def get_final_pred_kps(valkps, preheatmap, metainfo, outres):
    for i in range(preheatmap.shape[0]):
        prehmap = preheatmap[i, :, :, :]
        meta = metainfo[i]
        sample_index = meta['sample_index']
        meta_kps = meta['pts']
        kps = get_predicted_kp_from_htmap(prehmap, meta, outres)
        valkps[sample_index, :, :] = np.concatenate((kps[:, 0:2], meta_kps[:, 0:2]), axis=1)  # ignore the visibility


def main_eval(modelfile, num_stack):
    inres = (192, 192)
    outres = (48, 48)

    xnet = keras.models.load_model(modelfile)

    valdata = MPIIDataGen("data/mpii/mpii_annotations.json", "data/mpii/images",
                          inres=inres, outres=outres, is_train=False)

    valkps = np.zeros(shape=(valdata.get_dataset_size(), 16, 4), dtype=np.float)

    count = 0
    batch_size = 8
    for _img, _gthmap, _meta in valdata.generator(batch_size, num_stack, sigma=1, is_shuffle=False, with_meta=True):

        count += batch_size

        if count > valdata.get_dataset_size():
            break

        out = xnet.predict(_img)

        get_final_pred_kps(valkps, out[-1], _meta, outres)

    
    savemat(modelfile + 'preds.mat', mdict={'preds': valkps})


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--gpuID", default=0, type=int, help='gpu id')
    parser.add_argument("--model_path", help='path to trained model')
    parser.add_argument("--num_stack", default=2, type=int, help='num of stacks in trained model')

    args = parser.parse_args()

    main_eval(modelfile=args.model_path, num_stack=args.num_stack)
