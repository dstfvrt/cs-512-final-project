#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 21:24:04 2020

@author: johnjz30
"""
def annotByImg():
    import json
    filename= 'anno.json' #mpii_annotations.json'
    with open(filename) as f:
      data = json.load(f)
    
      
    lengths={}; count=0; first=0
    for i in range(len(data)):
        image_path = data[i]['img_paths']
        if lengths.get(image_path) != None:
            lengths[image_path].append(data[i])
        else:
           lengths[image_path] = [data[i]]
    
    return lengths
       
       
def moveAnnot(annot, boxCoords):
        ## gives new coordinates relative to cropped dim.
        newCoords=[]
        for i in range(len(annot)):
            newCoords.append(annot[i]-boxCoords[i])
            
        return newCoords
        
def closestObj(img, lengths, center):
    ###
    dist=[]
    
    for obj in lengths[img]:
        dist.append((obj['objpos'][0]-center[0])**2 + (obj['objpos'][1]-center[1])**2)
    closest_index=np.argmin(dist)
    return closest_index
